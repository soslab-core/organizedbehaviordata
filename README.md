# OrganizedBehaviorData

This repository provides information about the data and supplemental information related to classifying organized behavior in collections of tweets.

The following article related to this work appeared in *WIMS ’18: 8th International Conference
on Web Intelligence, Mining and Semantics*:

```
Erdem Beğenilmiş and Suzan Uskudarli. 2018. Organized Behavior Classification of 
Tweet Sets using Supervised Learning Methods. In WIMS ’18: 8th International Conference
on Web Intelligence, Mining and Semantics, June 25–27, 2018, Novi Sad, Serbia. ACM,
New York, NY, USA, 9 pages.https://doi.org/10.1145/3227609.3227665
```

Further information about the data can be found [here]()
## Data Set - Collection Seed Tweets & Extended Tweets




Format of Extended Tweets File is as below : 

```
#StartAnalysis
Analysis Id|Collection Id|Traced Hashtag|Analyzed Tweets Date Interval|Organic - Organized Label|Political - NonPoliticalLabel|ProTrumpProHillary-NeitherLabel
Analysis1|Collection1|#exampleHashtag|Date1-Date2|ORGANIC|POLITICAL|TRUMP
#StartTweets
tweetId1
tweetId2
#EndTweets
#EndAnalysis
```

Tweet Set sizes can be found in : organizedBehaviorDataSets/direnajTweetSetSizes.csv

## Data Summary

Data summary can be found in the "dataSummary" directory.

### Data Size

Data size can be found in "totalCounts.txt" file.

## Training Data Sets (Extracted Features in each analysis)

Training Data sets used for classifications can be found in the "trainingDataSets" directory.

## Clustering Data Set
Clustering experiments can be found in "unsupervisedComparison" directory.

## Ira Data Set Analysis
Data, Experiment and analysis codes can be found in "iraDataSetAnalysis" directory.








